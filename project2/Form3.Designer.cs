﻿namespace project2
{
    partial class Form3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.chart1 = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.lab_Y_Axis = new System.Windows.Forms.Label();
            this.lab_X_Axis = new System.Windows.Forms.Label();
            this.lab_X_Axis_Cur = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // comboBox1
            // 
            this.comboBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(0, 0);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 0;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // chart1
            // 
            this.chart1.BackColor = System.Drawing.Color.Transparent;
            this.chart1.BackSecondaryColor = System.Drawing.Color.Black;
            this.chart1.BorderlineColor = System.Drawing.Color.Black;
            chartArea1.CursorX.IsUserEnabled = true;
            chartArea1.CursorX.IsUserSelectionEnabled = true;
            chartArea1.CursorY.IsUserEnabled = true;
            chartArea1.CursorY.IsUserSelectionEnabled = true;
            chartArea1.Name = "ChartArea1";
            this.chart1.ChartAreas.Add(chartArea1);
            this.chart1.Cursor = System.Windows.Forms.Cursors.Cross;
            this.chart1.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Name = "Legend1";
            this.chart1.Legends.Add(legend1);
            this.chart1.Location = new System.Drawing.Point(0, 0);
            this.chart1.Margin = new System.Windows.Forms.Padding(100);
            this.chart1.Name = "chart1";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chart1.Series.Add(series1);
            this.chart1.Size = new System.Drawing.Size(603, 294);
            this.chart1.TabIndex = 2;
            this.chart1.Text = "chart1";
            this.chart1.Click += new System.EventHandler(this.chart1_Click);
            this.chart1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.chart1_MouseMove);
            // 
            // lab_Y_Axis
            // 
            this.lab_Y_Axis.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lab_Y_Axis.Location = new System.Drawing.Point(277, -194);
            this.lab_Y_Axis.Name = "lab_Y_Axis";
            this.lab_Y_Axis.Size = new System.Drawing.Size(1, 1000);
            this.lab_Y_Axis.TabIndex = 3;
            this.lab_Y_Axis.Click += new System.EventHandler(this.lab_Y_Axis_Click);
            // 
            // lab_X_Axis
            // 
            this.lab_X_Axis.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.lab_X_Axis.Location = new System.Drawing.Point(-1017, 71);
            this.lab_X_Axis.Name = "lab_X_Axis";
            this.lab_X_Axis.Size = new System.Drawing.Size(2000, 1);
            this.lab_X_Axis.TabIndex = 4;
            // 
            // lab_X_Axis_Cur
            // 
            this.lab_X_Axis_Cur.AutoSize = true;
            this.lab_X_Axis_Cur.Location = new System.Drawing.Point(343, 72);
            this.lab_X_Axis_Cur.Name = "lab_X_Axis_Cur";
            this.lab_X_Axis_Cur.Size = new System.Drawing.Size(35, 13);
            this.lab_X_Axis_Cur.TabIndex = 5;
            this.lab_X_Axis_Cur.Text = "label1";
            this.lab_X_Axis_Cur.MouseMove += new System.Windows.Forms.MouseEventHandler(this.lab_X_Axis_Cur_MouseMove);
            // 
            // Form3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 294);
            this.Controls.Add(this.lab_X_Axis_Cur);
            this.Controls.Add(this.lab_X_Axis);
            this.Controls.Add(this.lab_Y_Axis);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.chart1);
            this.Name = "Form3";
            this.Text = "Form3";
            ((System.ComponentModel.ISupportInitialize)(this.chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.DataVisualization.Charting.Chart chart1;
        private System.Windows.Forms.Label lab_Y_Axis;
        private System.Windows.Forms.Label lab_X_Axis;
        private System.Windows.Forms.Label lab_X_Axis_Cur;
    }
}