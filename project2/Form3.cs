﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.Diagnostics;
using System.Windows.Forms.DataVisualization.Charting;
using System.Web;

using System.IO;
using System.Globalization;

namespace project2
{
    public partial class Form3 : Form
    {
        //List<float> all = new List<float>();
        float strt_m = 0; //name_of_profiling[1]
        float stop_m = 0; //name_of_profiling[name_of_profiling.Count-1]
        int how_many_profile = 0;
        float min_X = 100000;
        float max_X = 0;
        List<String> name_of_profiling = new List<String>();
        List<float> data = new List<float>();

        public Form3()
        {
            InitializeComponent();
            chart1.ChartAreas[0].AxisX.ScaleView.Zoomable = true;
            chart1.ChartAreas[0].AxisY.ScaleView.Zoomable = true;
            loadFile();
            comboBox1.Text = name_of_profiling[1];
        }

        public void loadFile()
        {
            StreamReader sr = new StreamReader(Form1.file_name_to_load+".txt");
            String lineOfData;

            while ((lineOfData = sr.ReadLine()) != null)
            {
                if (lineOfData == "STRT,M")
                {
                    strt_m = float.Parse(sr.ReadLine());
                }

                if(lineOfData == "STOP,M")
                {
                    stop_m = float.Parse(sr.ReadLine());
                }

                if (lineOfData == "~A DEPTH")
                {
                    while (lineOfData != "#")
                    {
                        name_of_profiling.Add(lineOfData);
                        lineOfData = sr.ReadLine();
                    }
                }

                if(lineOfData == "#")
                {
                    while ((lineOfData = sr.ReadLine()) != null)
                    {
                        data.Add(float.Parse(lineOfData));
                    }
                }
            }

            how_many_profile = name_of_profiling.Count - 1;

            for (int i = 1; i <= how_many_profile; i++)
            {
                comboBox1.Items.Add(name_of_profiling[i]);
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //label1.Text = how_many_profile.ToString();
            drawChart();
            //label1.Text = name_of_profiling.IndexOf(comboBox1.Text).ToString();
        }

        private void chart1_Click(object sender, EventArgs e)
        {
        }

        public void drawChart()
        {
            chart1.Series.Clear();

            var chart = chart1.ChartAreas[0];
            chart.AxisX.IntervalType = DateTimeIntervalType.Number;

            chart.AxisX.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.Format = "";
            chart.AxisY.LabelStyle.IsEndLabelVisible = true;

            chart1.Series.Add(comboBox1.Text);
            chart1.Series[comboBox1.Text].ChartType = SeriesChartType.Spline;
            max_X = 0;
            min_X = 10000;
            for (int i = name_of_profiling.IndexOf(comboBox1.Text); i < data.Count()/(how_many_profile+1); i++)
            {

                if (data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i-1)] != 999.0000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i-1)] != -999.0000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != 999.000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != -999.000)
                {
                    chart1.Series[comboBox1.Text].Points.AddXY(data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i-1)], data[0 + (how_many_profile + 1) * (i-1)]);
                }

                if (data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] < min_X && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != 999.0000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != -999.0000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != 999.000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != -999.000)
                {
                    min_X = data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)];
                }

                if (data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] > max_X && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != 999.0000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != -999.0000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != 999.000 && data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)] != -999.000)
                {
                    max_X = data[name_of_profiling.IndexOf(comboBox1.Text) + (how_many_profile + 1) * (i - 1)];
                }
            }

            chart.AxisX.Minimum = min_X;
            chart.AxisX.Maximum = max_X;
            chart.AxisY.Minimum = strt_m;
            chart.AxisY.Maximum = stop_m;
            chart.AxisX.Interval = 10; 
            chart.AxisY.Interval = 10;
        }

        private void lab_Y_Axis_Click(object sender, EventArgs e)
        {

        }

        private void lab_X_Axis_Cur_MouseMove(object sender, MouseEventArgs e)
        {

        }

        private void chart1_MouseMove(object sender, MouseEventArgs e)
        {
            lab_Y_Axis.Location = new Point((e.X+1),0);
            lab_X_Axis.Location = new Point(0, e.Y+1);

            try
            {
                double yValue = chart1.ChartAreas[0].AxisY.PixelPositionToValue(e.Y);
                double xValue = chart1.ChartAreas[0].AxisX.PixelPositionToValue(e.X);

                lab_X_Axis_Cur.Text = String.Concat(String.Concat(Math.Round(xValue, 1).ToString(), " , "), Math.Round(yValue, 1).ToString());

                lab_X_Axis_Cur.Location = new Point(755,e.Y - 5);
            }
            catch
            {

            }
            finally
            {

            }
        }
    }
}
