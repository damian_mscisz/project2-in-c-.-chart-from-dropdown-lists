﻿namespace project2
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label_file_to_load = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.button_send_form1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label_file_to_load
            // 
            this.label_file_to_load.Dock = System.Windows.Forms.DockStyle.Top;
            this.label_file_to_load.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label_file_to_load.Location = new System.Drawing.Point(0, 0);
            this.label_file_to_load.Name = "label_file_to_load";
            this.label_file_to_load.Size = new System.Drawing.Size(536, 22);
            this.label_file_to_load.TabIndex = 0;
            this.label_file_to_load.Text = "Wybierz plik który chcesz załadować";
            this.label_file_to_load.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // comboBox1
            // 
            this.comboBox1.AllowDrop = true;
            this.comboBox1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(196, 42);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 21);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.Text = "Wybierz";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // button_send_form1
            // 
            this.button_send_form1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.button_send_form1.AutoSize = true;
            this.button_send_form1.Location = new System.Drawing.Point(217, 101);
            this.button_send_form1.Name = "button_send_form1";
            this.button_send_form1.Size = new System.Drawing.Size(76, 23);
            this.button_send_form1.TabIndex = 2;
            this.button_send_form1.Text = "Przejdź dalej";
            this.button_send_form1.UseVisualStyleBackColor = true;
            this.button_send_form1.Click += new System.EventHandler(this.button_send_form1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(536, 286);
            this.Controls.Add(this.button_send_form1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label_file_to_load);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label_file_to_load;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button button_send_form1;
    }
}

