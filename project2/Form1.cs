﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace project2
{
    public partial class Form1 : Form
    {
        public static String file_name_to_load = "";

        public Form1()
        {
            InitializeComponent();

            comboBox1.Items.Add("K");
            comboBox1.Items.Add("W");
            comboBox1.Items.Add("SOB");
        }

        private void button_send_form1_Click(object sender, EventArgs e)
        {
            file_name_to_load = comboBox1.Text;
            this.Hide();
            Form3 f3 = new Form3();
            f3.ShowDialog();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }
    }
}
