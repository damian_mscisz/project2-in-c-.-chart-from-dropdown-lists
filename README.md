pliki wejsciowe .las

na początku formatka do wczytania interesujacego nas pliku.( nie okreslil czy na osobnym oknie przed głownym czy wszystko w jednym oknie)

----------------------------------------------------------

Z pliku nalezy wczytac wartosci( np. plik k.las):

STRT.M 2170.000: - wart. startowa glebokosci 
STOP.M 2381.000: - koncowa glebokosc 
STEP.M .250: - krok

UWAGA: te ":" na koncu wartosci moga byc sklejjone 2170.000: ale moze byc tez 2170.000 :

Powyzsze wartosci służą do okrślenia zbioru osi X (wartosc min, max i krok co daje nam caly zbior wartosci X)

NULL. -999.000 - wartosc nie zmieżona (ponizej wyjasnienie*) 

-----------------------------------------------------------
nastepnie przejsc do:
~A DEPT TENS CALM DPHI RHOB DRHO NPHI NPHS NPHD GR DTM SPHI ITT 

wczytać dane pod tymi nagłowkami, są to wyniki poszczególnych profilowań geofizycznych. W pliku pod naglowkami mogą pojawić się jednostki, wtedy też trzeba je pominąć. DEPTH to podane wcześniej X'y, nie trzeba ich wczytywać jeśli na podstawie tamtych 3 lini określimy je wcześniej. Każde kolejna koloumna to osobne profilwoanie (y1,y2..) i reprezentuje osobny wykres. Wykresy mają być wybierane z dropdown menu albo coś podobnego. OS X (depth) jest w pionie -> wzrost głebokości w dól. Oś Y: minimum i maximum wykresu ma byc dostosowane do wartosci min i max w danym profilowaniu. 

*Wartości NULL'a czyli w tym przypadku -999.000, jesli pojawia sie w danych profilowania to znaczy ze w wym miejscu nie przeprowadzono poprawnego pomiaru i te miejsca maja byc puste w wykresie. 

Osie maja mieć możliwość przełącznia na skale logarytmiczną (pod warunkiem, że nie występują wartości ujemne- > wtedy ta funkcja jest zablokowana)

Jeśli wykres nie mieści się w oknie programu (w pionie) to ma być możliwość jego scrolowania (pasek do skrolowania z boku)

Można też zrobić textbox w którym wpisywane są współrzędne kursora myszy na wykresie (według wartosci X,Y profilwoania) co ulatwia odczyt wartosci profilowania.